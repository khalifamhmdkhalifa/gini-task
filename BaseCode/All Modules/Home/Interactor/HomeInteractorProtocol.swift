//
//  HomeInteractorProtocol.swift
//  BaseCode
//
//  Created by khalifa on 4/21/20.
//  Copyright © 2020 khalifa. All rights reserved.
//

import Foundation

protocol HomeInteractorProtocol: class {
    func loadCats(page: Int, completion: @escaping ([CatModel]?, Error?)->Void)
    func addToFavourites(model: CatModel, completion: @escaping (Bool)-> Void)
    func removeFromFavourites(model: CatModel, completion: @escaping (Bool)-> Void)
    
}

class HomeInteractor: HomeInteractorProtocol {
    func removeFromFavourites(model: CatModel, completion: @escaping (Bool) -> Void) {
        Favourites.shared.removeFromFavourites(model: model, completion: completion)
    }
    
    func addToFavourites(model: CatModel, completion: @escaping (Bool)-> Void) {
        Favourites.shared.addToFavourites(model: model, completion: completion)
    }
    
    func loadCats(page: Int, completion: @escaping ([CatModel]?, Error?)-> Void) {
        CoreNetwork.sharedInstance.loadCats(page: page, completion: completion)
    }
    
}

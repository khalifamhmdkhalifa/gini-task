
import Foundation

extension Date {
    
    func getSecondsTillDate(date: Date) -> Int? {
        let start = self
        let end = date
        let calendar = Calendar.current
        let unitFlags = Set<Calendar.Component>([ .second])
        let datecomponents = calendar.dateComponents(unitFlags, from: start, to: end)
        let seconds = datecomponents.second
        return seconds
    }
}

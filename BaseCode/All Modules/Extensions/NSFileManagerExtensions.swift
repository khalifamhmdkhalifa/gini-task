
import UIKit

extension FileManager {
    
    func getDocumentDirectoryPath() -> String {
        return NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    }
    
    func createDirectoryAtPath(_ path: String) -> Bool {
        if FileManager.default.fileExists(atPath: path) {
            return false
        }
        do {
            try FileManager.default.createDirectory(atPath: path, withIntermediateDirectories: false, attributes: nil)
        } catch let error as NSError {
            print("Unable to create directory \(error.debugDescription)")
            return false
        }
        return true
    }
    
    func getObjectsInDirectory(_ path: String) -> [AnyObject] {
        let url = URL(fileURLWithPath: path)
        if let objects = try? FileManager.default.contentsOfDirectory(at: url, includingPropertiesForKeys: nil, options: FileManager.DirectoryEnumerationOptions.skipsSubdirectoryDescendants) {
            return objects as [AnyObject]
        }
        return [AnyObject]()
    }
    
    func createReadableAndWritableFileAtPath(_ path: String) -> Bool {
        // https://developer.apple.com/library/ios/documentation/Swift/Conceptual/Swift_Programming_Language/LexicalStructure.html#//apple_ref/swift/grammar/octal-literal
        let attributes: [FileAttributeKey:AnyObject] = [FileAttributeKey.posixPermissions : NSNumber(value: 0o660 as Int16), FileAttributeKey.protectionKey : FileProtectionType.none as AnyObject]
        let success = createFile(atPath: path, contents: nil, attributes: attributes)
        return success
    }
    
    func createAndOpenFile(_ path: String) -> FileHandle? {
        if createReadableAndWritableFileAtPath(path) {
            if isWritableFile(atPath: path) {
                if isReadableFile(atPath: path) {
                    return FileHandle(forWritingAtPath: path)
                } else {
                    NSLog("Unable to create readable file at path \(path)")
                }
            } else {
                NSLog("Unable to create writable file at path \(path)")
            }
        }  else {
            NSLog("Unable to create file at path \(path)")
        }
        return nil
    }
    /**
     *  Returns the full path instead of just the filename.
     */
    
    func enumerateFilesInDirectory(_ directoryAtPath: String, evaluator: (String) -> ()) {
        if let enumerator = enumerator(atPath: directoryAtPath) {
            let directoryUrl = URL(fileURLWithPath: directoryAtPath, isDirectory: true)
            for file in enumerator {
                if let filename = file as? String {
                        evaluator(directoryUrl.appendingPathComponent(filename).path)
                }
            }
        }
    }
    
    func getFilesInDirectory(_ directoryAtPath: String) -> [String] {
        var array = [String]()
        enumerateFilesInDirectory(directoryAtPath, evaluator: { array.append($0) })
        return array
    }
    
    func readFile(_ file: String) -> Data? {
        if !fileExists(atPath: file) {
            NSLog("Could not read file, doesnt exist: '\(file)'")
            return nil
        } else if !isReadableFile(atPath: file) {
            NSLog("Could not read file, file exists but not readable: '\(file)'")
            return nil
        } else {
            if let data = try? Data(contentsOf: URL(fileURLWithPath: file)) {
                return data
            } else {
                NSLog("Could not read file, unable to convert contents to NSData: '\(file)'")
                return nil
            }
        }
    }
    
    func removeFile(_ file: String) -> Bool {
        do {
            try removeItem(atPath: file)
            return true
        } catch {
            return false
        }
    }
    
    func attributesOfFile(_ file: String) -> [AnyHashable: Any]? {
        do {
            let attributes = try self.attributesOfItem(atPath: file)
            return attributes
        } catch  {
            return nil
        }
    }
}

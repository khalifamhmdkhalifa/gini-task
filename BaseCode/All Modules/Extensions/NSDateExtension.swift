
import UIKit

extension Date {
    
    func isLessThanDate(_ dateToCompare: Date) -> Bool {
        var isLess = false
        if self.compare(dateToCompare) == ComparisonResult.orderedAscending {
            isLess = true
        }
        
        return isLess
    }
    
    static func birthDateFrom(str: String) -> Date? {
        return Date.formatter().date(from: str)
    }
    
    static func birthDateString(date: Date?) -> String {
        guard let newDate = date else { return "" }
        return Date.formatter().string(from: newDate)
    }
    
    static func formatter() -> DateFormatter {
        let formatter = DateFormatter();
        formatter.timeZone = .current
        formatter.dateFormat = "yyyy-MM-dd";
        return formatter
    }
    
}

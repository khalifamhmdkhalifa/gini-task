
import UIKit

extension String {
    
    static var Empty: String { return "" }
    
    func removeOccurences() -> String {
        return configureString(self).localized()
    }
    
    fileprivate func configureString(_ title: String) -> String {
        var configuredStr = title.capitalized
        configuredStr = configuredStr.replacingOccurrences(of: "_", with: " ")
        return configuredStr
    }
    
    func localized() -> String {
        return NSLocalizedString(self, comment: "")
    }
    
}

extension String {
    
    
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }

        return String(data: data, encoding: .utf8)
    }

    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,20}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func urlValueFor(_ key: String) -> String? {
        let urlComponents = URLComponents(string: self)
        let queryItems = urlComponents?.queryItems
        let result = queryItems?.filter({$0.name == key}).first
        
        return result?.value
    }
    
    func splitBy(length: Int) -> String {
        var text = self
        if self.count > length {
            let indexAt = self.index(text.startIndex, offsetBy: length)
            let range: Range = indexAt..<text.endIndex
            text.removeSubrange(range)
        }
        
        return text
    }

    var isReallyEmpty: Bool {
        return self.trimmingCharacters(in: .whitespaces).count == 0
    }
    
    func guessLanguage() -> String {
        let length = self.utf16.count
        let languageCode = CFStringTokenizerCopyBestStringLanguage(self as CFString, CFRange(location: 0, length: length)) as String? ?? ""
        let locale = Locale(identifier: languageCode)
        return locale.localizedString(forLanguageCode: languageCode) ?? "Unknown"
    }
    
    public var replacedArabicDigitsWithEnglish: String {
        var str = self
        let map = ["٠": "0",
                   "١": "1",
                   "٢": "2",
                   "٣": "3",
                   "٤": "4",
                   "٥": "5",
                   "٦": "6",
                   "٧": "7",
                   "٨": "8",
                   "٩": "9",
                   ".": ".",
                   ",": ""]
        map.forEach { str = str.replacingOccurrences(of: $0, with: $1) }
        return str
    }
}

extension String {
    mutating func stringByRemovingRegexMatches(pattern: String, replaceWith: String = "") {
        do {
            let regex = try NSRegularExpression(pattern: pattern, options: NSRegularExpression.Options.caseInsensitive)
            let range = NSMakeRange(0, self.count)
            self = regex.stringByReplacingMatches(in: self, options: [], range: range, withTemplate: replaceWith)
        } catch {
            return
        }
    }
}

extension String {
    func truncate(length: Int, trailing: String = "…") -> String {
        if self.count > length {
            return String(self.prefix(length)) + trailing
        } else {
            return self
        }
    }
}


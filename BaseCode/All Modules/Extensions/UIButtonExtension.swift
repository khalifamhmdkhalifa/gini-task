//
//  UIButtonExtension.swift
//  Mumzworld
//
//  Created by Radu Nunu on 4/14/16.
//  Copyright © 2016 Yopeso. All rights reserved.
//

import UIKit

extension UIButton {
    
    func setRoundLayer() {
        self.layer.cornerRadius = self.frame.width / 2.0
        self.backgroundColor = UIColor.white
        self.clipsToBounds = true
    }
    
    @IBInspectable var xibLocKey: String? {
        get { return nil }
        set(key) {
            self.setTitle(key?.localized(), for: .normal)
        }
    }
}

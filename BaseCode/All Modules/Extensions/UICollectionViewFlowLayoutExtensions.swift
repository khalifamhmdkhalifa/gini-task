//
//  UICollectionViewFlowLayoutExtensions.swift
//  Mumzworld
//
//  Created by Khalifa on 4/16/19.
//  Copyright © 2019 Softxpert. All rights reserved.
//

import UIKit

extension UICollectionViewFlowLayout {
    open override var flipsHorizontallyInOppositeLayoutDirection: Bool {
        return true
    }
}

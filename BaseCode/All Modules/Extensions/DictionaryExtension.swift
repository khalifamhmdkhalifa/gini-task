
import UIKit

extension Dictionary {
    mutating func add(_ other: Dictionary) {
        for (key, value) in other {
            self.updateValue(value, forKey: key)
        }
    }
    
    func writeDictionaryTo(path: String) {
        NSDictionary(dictionary: self).write(toFile: path, atomically: true)
    }
    
}

func += <K, V> (left: inout [K:V], right: [K:V]) {
    for (k, v) in right {
        left.updateValue(v, forKey: k)
    }
}

func + <K,V> (left: Dictionary<K,V>, right: Dictionary<K,V>?) -> Dictionary<K,V> {
    guard let right = right else { return left }
    return left.reduce(right) {
        var new = $0 as [K:V]
        new.updateValue($1.1, forKey: $1.0)
        return new
    }
}

extension NSDictionary {
    
    func swiftDictionary() -> [String: Any] {
        guard let swiftDict = self as? [String: Any] else { return [:] }
        return swiftDict
    }
    
}

//
//  CatRequester.swift
//  BaseCode
//
//  Created by khalifa on 7/24/20.
//  Copyright © 2020 khalifa. All rights reserved.
//

import Foundation

class CatRequester {
    private static let serchUrl = "images/search"
    private let communication: NetworkingInterface
    private let limit = 50
    
    init(communication: NetworkingInterface) {
        self.communication = communication
    }
    
    func loadCats(page: Int, completion: @escaping ([CatModel]?, Error?)-> Void ) {
        let requestSpecs = RequestSpecs<[CatModel]>(method: .GET, URLString: CatRequester.serchUrl, parameters: ["page": page as AnyObject, "limit": 50 as AnyObject ])
        communication.request(requestSpecs, completionBlock: completion)
    }
}


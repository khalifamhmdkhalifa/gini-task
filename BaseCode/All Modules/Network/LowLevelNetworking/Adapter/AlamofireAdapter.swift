//
//  AlamofireAdapter.swift
//  ios-task
//
//  Created by Khalifa on 10/3/18.
//  Copyright © 2018 khalifa. All rights reserved.
//

import UIKit
import Alamofire

class AlamofireAdaptor: NetworkingInterface {
    let requestBaseURL: String
    let requestHeaders: [String: String]?
    let minValidStatusCode = 200
    let maxValidStatusCode = 403
    let AlamofireBugErrorCode = 3840
    let validContentTypes = ["application/json", "Accept", "application/hal+json"]
    let backgroundQueue = DispatchQueue(label: "com.app.requests", qos: .userInteractive)
    
    init(baseURL: String , headers: [String: String]? = nil, authentication: AuthenticationSpecs? = nil) {
        self.requestBaseURL = baseURL
        self.requestHeaders = headers
    }
    
    
    func isConnectedToInternet() -> Bool {
        let reachable =  Alamofire.NetworkReachabilityManager()?.isReachable
        return reachable ?? false
    }
    
    func authRequest<T:Decodable>(_ specs: RequestSpecs<T>, completionBlock: @escaping (T?,Error?)->Void) {
    }
    
    func request<T:Decodable>(_ specs: RequestSpecs<T>, completionBlock: @escaping (T?, Error?) -> (Void)) {
        //if !isConnectedToInternet() { return }
        let url = requestBaseURL + specs.URLString
        let encoding = convertRequestEnconding(specs.encoding)
        let request = Alamofire.request(url, method: convertRequestMethodToAlamofireMethod(specs.method),
                                        parameters: specs.parameters, encoding: encoding, headers: requestHeaders)
            .validate(contentType: validContentTypes)
        backgroundQueue.async {
            request.responseJSON() { response in
                if let requestURL = response.request?.url?.absoluteString {
                    print("Request - \(requestURL)")
                }
                guard let data = response.data else{
                    completionBlock(nil,response.error)
                    return
                }
                do {
                    let result = try JSONDecoder().decode(T.self, from: data)
                    completionBlock(result,response.error)
                } catch let decodeError {
                    completionBlock(nil,decodeError)
                }
            }
        }
    }
    
    
    func convertRequestEnconding(_ enconding: Encoding) -> ParameterEncoding {
        switch enconding {
        case .json:
            return JSONEncoding.default
        case.urlEncodedInURL:
            return URLEncoding.default
        }
    }
    
    func convertRequestMethodToAlamofireMethod(_ method: RequestMethod) -> Alamofire.HTTPMethod {
        switch method {
        case .DELETE :
            return HTTPMethod.delete
        case .GET :
            return HTTPMethod.get
        case .PUT :
            return HTTPMethod.put
        case .POST :
            return HTTPMethod.post
        }
    }
    
    struct AuthenticationSpecs {
        let userName: String
        let password: String
    }
    
    func downloadImage(url: URL, onResult: @escaping (UIImage?, Error?) -> Void) {
        getDataFromUrl(url: url) {data, response, error in
            guard let data = data, error == nil else {
                DispatchQueue.main.async() {
                    onResult(nil,error)
                }
                return
            }
            let img = UIImage(data: data)
            DispatchQueue.main.async() {
                onResult(img,error)
            }
        }
    }
    
    func getDataFromUrl(url: URL,
                        completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        Alamofire.request(url).response(){ response in
            completion(response.data,response.response,response.error)
            
        }
    }
}

//
//  CoreNetwork.swift
//  ios-task
//
//  Created by Khalifa on 10/3/18.
//  Copyright © 2018 khalifa. All rights reserved.
//

import UIKit

class CoreNetwork {
    static var sharedInstance = CoreNetwork()
    
    fileprivate lazy var networkCommunication: NetworkingInterface = {
        AlamofireAdaptor(baseURL: HostService.getBaseURL() ,headers: HostService.headers, authentication: nil)
    }()
    
    func reloadInstances() {
         networkCommunication = AlamofireAdaptor(baseURL: HostService.getBaseURL() , headers: nil, authentication: nil)
    }
    
    func downloadImage(url: URL,onResult: @escaping (UIImage? ,Error?)->Void) {
        networkCommunication.downloadImage(url: url, onResult: onResult)
    }
    
    func isConnectedToNetwork() -> Bool {
        return networkCommunication.isConnectedToInternet()
    }
    
}

extension CoreNetwork {
    func loadCats(page: Int, completion: @escaping ([CatModel]?, Error?)->Void) {
        CatRequester.init(communication: networkCommunication).loadCats(page: page, completion: completion)
    }
}

//
//  NetworkInteractor.swift
//  BaseCode
//
//  Created by khalifa on 4/21/20.
//  Copyright © 2020 khalifa. All rights reserved.
//

import Foundation

protocol NetworkInteractor {
    var networkConnection: CoreNetwork {set get}
    init(networkConnection: CoreNetwork)
}


//
//  BasePresenter.swift
//  BaseCode
//
//  Created by khalifa on 4/21/20.
//  Copyright © 2020 khalifa. All rights reserved.
//

import Foundation

class BasePresenter<View, Interactor> {
    let view: View?
    let interactor: Interactor?
    
    init(view: View, interactor: Interactor) {
        self.view = view
        self.interactor = interactor
    }
    
    func viewDidLoad() {
        
    }
    
    func viewWillAppear() {
        
    }
    
    func viewDidAppear() {
        
    }
    
    func viewWillDisappear() {
        
    }
    
    func viewDidDisappear() {
        
    }
}

extension BasePresenter: BasePresenterProtocol {    
}

//
//  SceneDelegate.swift
//  BaseCode
//
//  Created by khalifa on 4/21/20.
//  Copyright © 2020 khalifa. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    
    
    @available(iOS 13.0, *)
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "HomeViewController", bundle: nil)
        guard let initialViewControlleripad: HomeViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController else { return }
        let interactor = HomeInteractor()
        let presenter = HomePresenter(view:initialViewControlleripad , interactor: interactor)
        initialViewControlleripad.presenter = presenter
        self.window?.rootViewController = UINavigationController.init(rootViewController:  initialViewControlleripad)
        self.window?.makeKeyAndVisible()
        guard let _ = (scene as? UIWindowScene) else { return }
    }
}

